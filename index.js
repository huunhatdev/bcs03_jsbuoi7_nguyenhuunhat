var array = [];

var arrReal = [];
var arrSort = [];
var arrTemp = [];
// document.getElementById("array").innerHTML = array;

function addItem() {
  var num = document.getElementById("txt-number")?.value * 1;
  if (num || num == 0) {
    array.push(num);
    arrSort.push(num);
    arrTemp.push(num);
  }
  // console.log(array);
  var sumPositive = 0;
  var arrPositive = [];
  var arrNegative = [];
  var minNumber = num;
  var minPositive = num;
  var numEvenLast = -1;
  var prime = -1;
  var flag1 = true;
  //sắp xếp tăng dần

  array.forEach((number, i) => {
    if (number > 0) {
      sumPositive += number;
      arrPositive.push(number);
      minPositive =
        minPositive <= 0 ? number : number < minPositive ? number : minPositive;
    } else {
      arrNegative.push(number);
    }

    //Min Number
    minNumber = number < minNumber ? number : minNumber;

    //số chẵn cuối cùng trong mảng
    numEvenLast = number % 2 === 0 ? number : numEvenLast;

    //số nguyên tố đầu tiên trong mảng
    if (number > 1 && flag1) {
      //đặt cờ nếu không phải snt sẽ thoát vòng lặp
      var isBreak = false;
      for (let u = 2; u < number; u++) {
        isBreak = false;
        if (number % u === 0) {
          isBreak = true;
          break;
        }
      }

      if (!isBreak || number === 2) {
        prime = number;
        flag1 = false;
      }
    }
  });

  // sắp xếp tăng dần
  var sort = arrSort.sort((a, b) => {
    return a - b;
  });

  console.log(sort);

  //so sánh số lượng số dương và số âm
  var posVsNeg = null;
  if (arrPositive.length === arrNegative.length) {
    posVsNeg = `=`;
  } else if (arrPositive.length > arrNegative.length) {
    posVsNeg = `>`;
  } else {
    posVsNeg = `<`;
  }

  minPositive = minPositive < 0 ? `Không có số dương nào` : minPositive;
  document.getElementById("result1").innerHTML = `<p>
      1. Tổng các số dương: ${sumPositive}
    </p>
    <p>2. Đếm số dương: ${arrPositive.length}</p>
    <p>3. Số nhỏ nhất trong mảng: ${minNumber}</p>
    <p>4. Số dương nhỏ nhất: ${minPositive}</p>
    <p>5. Số chẵn cuối cùng trong mảng: ${numEvenLast}</p>
    <p>7. Sắp xếp tăng dần: ${sort}</p>
    <p>8. Số nguyên tố đầu tiên: ${prime}</p>
    
    <p>10. So sánh số lượng: số dương ${posVsNeg} số âm</p>`;

  document.getElementById("array").innerHTML = array;
  //   console.log({ countNegative, sumNegative });
}

function swap() {
  var i1 = document.getElementById("txt-index-1")?.value * 1;
  var i2 = document.getElementById("txt-index-2")?.value * 1;
  var temp = 0;
  var length = arrTemp.length;
  var result = null;

  if (i1 >= length || i1 < 0 || i2 >= length || i2 < 0) {
    result = `Bạn nhập giá trị không hợp lệ`;
  } else {
    temp = arrTemp[i1];
    arrTemp[i1] = arrTemp[i2];
    arrTemp[i2] = temp;
    result = arrTemp;
  }

  document.getElementById("result-swap").innerHTML = result;
}

function addItemReal() {
  var num = document.getElementById("txt-number-real")?.value * 1;
  var arrInt = [];
  if (num || num === 0) {
    arrReal.push(num);
  }

  arrReal.forEach((n, i) => {
    if (Number.isInteger(n)) {
      arrInt.push(n);
    }
  });

  console.log(arrReal);

  document.getElementById("result-real").innerHTML = `
  <p>Mảng: ${arrReal}</p>
  <p>9. Tổng số nguyên trong mảng: ${arrInt.length}</p>`;
}

//convert số chẵn về phía mảng trái tăng dần, số lẻ về phía phải giảm dần
// dùng 1 mảng phụ
// không dùng mảng phụ
// [1,4,11,44,3,22,55,5]
